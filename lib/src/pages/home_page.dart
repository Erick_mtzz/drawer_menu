import 'package:drawer_app/src/pages/first.dart';
import 'package:drawer_app/src/pages/second.dart';
import 'package:flutter/material.dart';

import 'package:hidden_drawer_menu/hidden_drawer/hidden_drawer_menu.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ScreenHiddenDrawer> items = new List();

  @override
  void initState() {
    items.add(new ScreenHiddenDrawer(
        new ItemHiddenMenu(
          name: 'Pagina 1',
          baseStyle: TextStyle(color: Colors.white, fontSize: 28.0),
          selectedStyle: TextStyle(
              color: Colors.red,
              fontSize: 28.0,
              backgroundColor: Colors.black54),
          colorLineSelected: Colors.teal,
        ),
        FirstPage()));

    items.add(new ScreenHiddenDrawer(
        new ItemHiddenMenu(
          name: 'Pagina 2',
          baseStyle: TextStyle(color: Colors.white, fontSize: 28.0),
          selectedStyle: TextStyle(
              color: Colors.red,
              fontSize: 28.0,
              backgroundColor: Colors.black54),
          colorLineSelected: Colors.orange,
        ),
        SecondPage()));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return HiddenDrawerMenu(
      backgroundColorMenu: Colors.black,
      backgroundColorAppBar: Colors.greenAccent,
      screens: items,
      // typeOpen: TypeOpen.FROM_RIGHT,
      // enableScaleAnimin: true,
      // enableCornerAnimin: true,
      slidePercent: 45.0,
      // verticalScalePercent: 60.0,
      contentCornerRadius: 20.0,
      iconMenuAppBar: Icon(Icons.view_module),
      //  backgroundColorContent: DecorationImage((image: ExactAssetImage('assets/bg_news.jpg'),fit: BoxFit.cover),
      // backgroundColorContent: Colors.black,
      backgroundMenu: DecorationImage(image: AssetImage('assets/fondo.png')),
      //    whithAutoTittleName: true,
      //    styleAutoTittleName: TextStyle(color: Colors.red),
      //    actionsAppBar: <Widget>[],
      //    backgroundColorContent: Colors.blue,
      //    elevationAppBar: 4.0,
      //    tittleAppBar: Center(child: Icon(Icons.ac_unit),),
      enableShadowItensMenu: true,
      //    backgroundMenu: DecorationImage(image: ExactAssetImage('assets/bg_news.jpg'),fit: BoxFit.cover),
    );
  }
}
